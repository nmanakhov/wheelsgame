﻿using UnityEngine;
using System.Collections;
using System;

[Serializable]
public class DimetricTransform : MonoBehaviour {
    public static float K = 0.5f;
    public Vector3 position;
    public bool marginLeft = false;
	void Start () {
        UpdatePosition();
	}

	public Vector3 Position
    {
        get {
            return position;
        }
        set {
            position = value;
            UpdatePosition();
        }
    }

    public void Move(float x, float y, float z)
    {
        position += new Vector3(x, y, z);
        UpdatePosition();
    }

    public void Move(Vector3 offset)
    {
        position += offset;
        UpdatePosition();
    }
    private Vector3 ViewPortLeft = new Vector3(0f,0.5f,0f);
    public void UpdatePosition()
    {
        Vector3 result = new Vector3(position.x, (position.y + position.z) * K, position.z);
        if (marginLeft)
        {
            Vector3 temp = Camera.main.ViewportToWorldPoint(ViewPortLeft);
            result.x += temp.x;
        }
        this.transform.position = result;   
    }

    public static Vector3 DimetricToWorldPosition(Vector3 v)
    {
        Vector3 result = new Vector3();
        result.x = v.x;
        result.y = (v.y + v.z) * K;
        result.z = v.z;
        return result;
    }
}

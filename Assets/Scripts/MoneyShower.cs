using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoneyShower : MonoBehaviour {
    private static MoneyShower instance_;
    public static MoneyShower Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<MoneyShower>();
            return instance_;
        }
    }
    Text text;
    public void Start()
    {
        UpdateMoney();
    }

    public void UpdateMoney()
    {
        text = this.GetComponent<Text>();
        text.text = " " + CoinController.Count;
    }
}

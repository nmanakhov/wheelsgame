﻿using UnityEngine;
using System.Collections;
using System;

public class SoundController : MonoBehaviour {
    AudioSource musics;
    AudioSource sounds;
    private static SoundController instance_;
    public static SoundController Instance
    {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<SoundController>();
            return instance_;
        }
    }
    public void Start()
    {
        if (instance_ != null && Instance != this)
            Destroy(this.gameObject);
        musics = gameObject.AddComponent<AudioSource>();
        sounds = gameObject.AddComponent<AudioSource>();
        DontDestroyOnLoad(this.gameObject);
    }

    public void SetVolume(float v)
    {
        musics.volume = v;
        sounds.volume = v;
    }

    public void PlaySound(AudioClip audioClip)
    {
        sounds.loop = false;
        sounds.clip = audioClip;
        sounds.Play();
    }
    public void PlayMusic(AudioClip audioClip)
    {
        musics.clip = audioClip;
        musics.Play();
        musics.loop = true;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CoinController : MonoBehaviour {
    static int count = 0;
	public static int Count
    {
        get
        {
            if (count == 0)
                count = PlayerPrefs.GetInt("Money");
            return count;
        }

        set
        {
            count = value;
            PlayerPrefs.SetInt("Money", value);
            PlayerPrefs.Save();
        }
    }
    public static void Decrease(int value)
    {
        Count -= value;
    }

    public static void Increase(int value)
    {
        Count += value;
    }
}


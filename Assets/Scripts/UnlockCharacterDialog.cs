﻿using UnityEngine;
using System.Collections;

public class UnlockCharacterDialog : MonoBehaviour {
    public UnlockPersonageButton unlockPersonageButton;
    public void Yes()
    {
        unlockPersonageButton.Unlock();
        this.gameObject.SetActive(false);
    }

    public void No()
    {
        this.gameObject.SetActive(false);
    }
}

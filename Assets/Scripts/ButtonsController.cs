﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonsController : MonoBehaviour {
    public AudioClip music;
    public PersonagePicker personagePicker;
    public void LoadScene(string name)
    {
        SceneManager.LoadScene(name);
    }

    public void Start()
    {
        SoundController.Instance.PlayMusic(music);
    }
    public void SetCoins(int num)
    {
        CoinController.Count = num;
    }
    public void LoadPickedScene()
    {
        string picked = personagePicker.triggers[personagePicker.pickedPersonage];
        if (PlayerPrefs.GetInt(picked) == 0)
            return;
        switch (picked)
        {
            case "girl":
                SceneManager.LoadScene("sc2");
                break;
            case "boy":
                SceneManager.LoadScene("sc1");
                break;
            case "man":
                SceneManager.LoadScene("sc3");
                break;
            case "woman":
                SceneManager.LoadScene("sc5");
                break;
            case "oldman":
                SceneManager.LoadScene("sc4");
                break;
        }
    }
}

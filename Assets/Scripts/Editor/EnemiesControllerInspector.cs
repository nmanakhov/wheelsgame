﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor(typeof(EnemiesController))]
public class EnemiesControllerInspector : Editor {
    Vector3[] coordinates = new Vector3[4];
    private Vector3 ViewPortLeft = new Vector3(0f, 0.5f, 0f);
    void OnSceneGUI()
    {
        EnemiesController enemiesController = (target as EnemiesController);
        coordinates[0] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, enemiesController.topBorder));
        coordinates[1] = DimetricTransform.DimetricToWorldPosition(new Vector3(enemiesController.rightBorder, 0f, enemiesController.topBorder));
        coordinates[2] = DimetricTransform.DimetricToWorldPosition(new Vector3(enemiesController.rightBorder, 0f, enemiesController.botBorder));
        coordinates[3] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, enemiesController.botBorder));
        Vector3 offset = Camera.main.ViewportToWorldPoint(ViewPortLeft);
        Handles.DrawLine(coordinates[0] + offset, coordinates[1] + offset);
        Handles.DrawLine(coordinates[1] + offset, coordinates[2] + offset);
        Handles.DrawLine(coordinates[2] + offset, coordinates[3] + offset);
    }
}

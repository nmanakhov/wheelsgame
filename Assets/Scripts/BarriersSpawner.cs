﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BarriersSpawner : MonoBehaviour {
    private static BarriersSpawner instance_;
    public static BarriersSpawner Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<BarriersSpawner>();
            return instance_;
        }
    }
    public float spawningBorder = 12f;
    public float cooldown = 10f;
    public float topBorder;
    public float botBorder;
    public float speed;
    public List<GameObject> prefabs;
    public int maxCountInAColoumn = 4;
    List<List<GameObject>> pool = new List<List<GameObject>>();
    public List<Vector3> possiblePositions;
    float timer;
    Transform barriers;
    bool gameOver = false;
    public void Start()
    {
        barriers = GameObject.Find("Barriers").transform;
        timer = cooldown;
        for (int i = 0; i < prefabs.Count; i++)
            pool.Add(new List<GameObject>());
    }
    public void GameOver()
    {
        gameOver = true;
    }

    public void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0f && possiblePositions.Count>0 && !gameOver)
        {
            timer = cooldown;
            Vector3 temp = possiblePositions[Random.Range(0, possiblePositions.Count)];
            temp.y = 0;
            temp.x = spawningBorder; 
            GetFromPool(Random.Range(0,prefabs.Count), temp, true);
        }
    }

    public GameObject GetFromPool(int num)
    {
        GameObject result;
        if (pool[num].Count > 0)
        {
            result = pool[num][0];
            pool[num].Remove(result);
        }
        else
        {
            result = Instantiate(prefabs[num]);
        }
        result.SetActive(true);
        result.transform.parent = barriers;
        return result;
    }

    public GameObject GetFromPool(int num, Vector3 position, bool saveY)
    {
        GameObject go = GetFromPool(num);
        DimetricTransform dT = go.GetComponent<DimetricTransform>();
        Vector3 temp = position;
        if (saveY)
            temp.y = dT.Position.y;
        dT.Position = temp;
        return go;
    }
    public void PutToPool(GameObject go)
    {
        go.SetActive(false);
        pool[go.GetComponent<Barrier>().Id].Add(go);
    }
}

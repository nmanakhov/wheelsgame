﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour {

    float score = 0f;
    BarriersSpawner barriersSpawner;
    Text text;
    public void Start()
    {
        barriersSpawner = BarriersSpawner.Instance;
        text = this.GetComponent<Text>();
    }
    public void Update()
    {
        score += BarriersSpawner.Instance.speed*Time.deltaTime;
        text.text = ((int)score).ToString();
    }

    public int GetScore()
    {
        return (int)score;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CarScript : MonoBehaviour {
    public float carSpeed = -5f;
    DimetricTransform dimetricTransform;
    void Start()
    {
        dimetricTransform = this.GetComponent<DimetricTransform>();
    }

    void Update () {
        dimetricTransform.Move(carSpeed * Time.deltaTime,0f,0f);
        if (dimetricTransform.Position.x < -60)
            CarPool.Instance.PutObject(this.gameObject);
	}
}

﻿using UnityEngine;
using System.Collections;

public class SoundPlayer : MonoBehaviour {

    public void PlaySound(AudioClip audioClip)
    {
        SoundController.Instance.PlaySound(audioClip);
    }

    public void PlayMusic(AudioClip audioClip)
    {
        SoundController.Instance.PlayMusic(audioClip);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FinalScoreShow : MonoBehaviour {
    public ScoreCounter scoreCounter;
    Text text;
    void Start () {
        int record = PlayerPrefs.GetInt("Record");
        text = GetComponent<Text>();
        if (record < scoreCounter.GetScore()) {
            text.text = "New record: " + scoreCounter.GetScore().ToString() + "! Congrats!";
            CoinController.Increase(10);
            PlayerPrefs.SetInt("Record", scoreCounter.GetScore());
        } else 
        text.text = "Your Score:" + scoreCounter.GetScore().ToString();
	}
	
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnlockPersonageButton : MonoBehaviour {
    public int id;
    public UnlockCharacterDialog dialog;
	void Start () {
	}
    public void ShowDialog()
    {
        PersonagePicker pP = transform.parent.parent.GetComponent<PersonagePicker>();
        if (CoinController.Count < pP.prices[id])
            return;
        dialog.unlockPersonageButton = this;
        dialog.gameObject.SetActive(true);
    }
    public void Unlock()
    {
        PersonagePicker pP = transform.parent.parent .GetComponent<PersonagePicker>();
        PlayerPrefs.SetInt(pP.triggers[id], 1);
        this.GetComponent<Image>().enabled = false;
        CoinController.Decrease(pP.prices[id]);
        MoneyShower.Instance.UpdateMoney();
    }
}

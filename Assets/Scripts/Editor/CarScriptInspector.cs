﻿using UnityEngine;
using UnityEditor;
using System.Collections;
[CustomEditor(typeof(CarPool))]
public class CarScriptInspector : Editor {
    Vector3[] coordinates = new Vector3[5];
    void OnSceneGUI()
    {
        CarPool carController = (target as CarPool);
        Handles.color = Color.red;
        coordinates[0] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, carController.topPosition));
        coordinates[1] = DimetricTransform.DimetricToWorldPosition(new Vector3(150, 0f, carController.topPosition));
        coordinates[2] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, carController.botPosition));
        coordinates[3] = DimetricTransform.DimetricToWorldPosition(new Vector3(150, 0f, carController.botPosition));
        Handles.DrawLine(coordinates[0], coordinates[1]);
        Handles.DrawLine(coordinates[2], coordinates[3]);
    }
}

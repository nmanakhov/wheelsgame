﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(DimetricTransform))]
public class PersonageController : MonoBehaviour {
    public float speed;
    public float topBorder = 10f;
    public float botBorder = -10f;
    public float boostSpeed = 20f;
    public float boostTime = 4f;
    public int linesNum;
    private List<Vector3> possiblePositions;
    int currentLine;
    public enum MouseStatement {
        mouseDown, mouseUp
    }
    public enum BoyStatement {
        idle, push, fastMoving,falling 
    }
    DimetricTransform dimetricTransform;
    MouseStatement statement = MouseStatement.mouseUp;
    BoyStatement bStatement = BoyStatement.idle;
    Animator animator;
    private float startSpeed = 0f;
    float timer;
    void Start() {
        currentLine = linesNum / 2;
        dimetricTransform = this.GetComponent<DimetricTransform>();
        animator = this.GetComponent<Animator>();
        startSpeed = BarriersSpawner.Instance.speed;
        Vector3 temp= dimetricTransform.Position;
        float dz = (topBorder - botBorder) / (linesNum-1);
        possiblePositions = new List<Vector3>();
        for (int i = 0; i < linesNum; i++)
        {
            possiblePositions.Add(new Vector3(temp.x, -0.4f , botBorder + dz * i));
        }
        BarriersSpawner.Instance.possiblePositions = this.possiblePositions;
    }

    void Update() {
        timer -= timer >= 0 ? Time.deltaTime : 0;
        CheckMouse();
        CheckPersonage();
    }

    private void CheckPersonage()
    {
        switch (bStatement)
        {
            case BoyStatement.idle:
                if (statement == MouseStatement.mouseDown)
                {
                    bStatement = BoyStatement.push;
                    animator.SetTrigger("Push");
                }
            break;
            case BoyStatement.push:
                if (statement == MouseStatement.mouseUp)
                {
                    bStatement = BoyStatement.idle;                    
                    animator.SetTrigger("Idle");
                }
            break;
            case BoyStatement.fastMoving:
                if (timer < 0)
                {
                    BarriersSpawner.Instance.speed = startSpeed;
                    bStatement = BoyStatement.idle;
                    transform.GetChild(0).GetComponent<Animator>().SetTrigger("Slow");
                }
                break;
        }
    }
    public AudioClip batteryPickSound;

    public void OnTriggerEnter(Collider coll)
    {
        switch (coll.tag)
        {
            case "Barrier":
                Fall();

                break;
            case "Money":
                CoinController.Increase(2);
                MoneyShower.Instance.UpdateMoney();
                BarriersSpawner.Instance.PutToPool(coll.gameObject);
                SoundController.Instance.PlaySound(batteryPickSound);
                break;
            case "SilverMoney":
                CoinController.Increase(1);
                MoneyShower.Instance.UpdateMoney();
                BarriersSpawner.Instance.PutToPool(coll.gameObject);
                SoundController.Instance.PlaySound(batteryPickSound);
                break;
            case "Bonus":
                Charge.Instance.IncreaseCharge(10);
                BarriersSpawner.Instance.PutToPool(coll.gameObject);
                SoundController.Instance.PlaySound(batteryPickSound);
                break;
            case "HugeBonus":
                Charge.Instance.IncreaseCharge(100);
                BarriersSpawner.Instance.PutToPool(coll.gameObject);
                SoundController.Instance.PlaySound(batteryPickSound);
                break;
        }
    }
    private Vector3 mouseDelta;
    private Vector3 lastMousePos;
    public void CheckMouse()
    {
        if (bStatement == BoyStatement.falling)
            return;
        else
            dimetricTransform.Position = Vector3.MoveTowards(dimetricTransform.Position, possiblePositions[currentLine], speed*Time.deltaTime);

        switch (statement)
        {
            case MouseStatement.mouseDown:
                if (Input.GetMouseButtonUp(0))
                {
                    mouseDelta = Camera.main.ScreenToViewportPoint(Input.mousePosition) - lastMousePos;
                    if (mouseDelta.y > 0.1f)
                        currentLine++;
                    if (mouseDelta.y < -0.1f)
                        currentLine--;
                        currentLine = Mathf.Clamp(currentLine, 0, possiblePositions.Count-1);
                    Debug.Log(mouseDelta);
                    statement = MouseStatement.mouseUp;
                }
            
                break;

            case MouseStatement.mouseUp:
                if (Input.GetMouseButtonDown(0))
                {
                    statement = MouseStatement.mouseDown;
                    lastMousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
                }
                break;
        }
    }

    public void Boost()
    {
        bStatement = BoyStatement.fastMoving;
        animator.SetTrigger("MoveFast");
        transform.GetChild(0).GetComponent<Animator>().SetTrigger("Boost");
        timer = boostTime;
        BarriersSpawner.Instance.speed = boostSpeed;
    }

    public GameObject gamePanel;
    public GameObject gameOverPanel;
    public void GameOver()
    {
        gamePanel = GameObject.Find("GameController").GetComponent<CharacterSpawner>().gamePanel;
        gamePanel.SetActive(false);
        gameOverPanel = GameObject.Find("GameController").GetComponent<CharacterSpawner>().gameOverPanel;
        gameOverPanel.SetActive(true);
        BarriersSpawner.Instance.GameOver();
    }
    public void Fall()
    {
        if (bStatement == BoyStatement.falling)
            return;     
        bStatement = BoyStatement.falling;
        animator.SetTrigger("Fall");
        BarriersSpawner.Instance.speed = 0f;
        GameOver();
        if (EnemiesController.Instance!=null)
        EnemiesController.Instance.GameOver();
    }
}

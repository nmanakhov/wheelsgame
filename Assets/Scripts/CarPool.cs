﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CarPool : MonoBehaviour {
    private static CarPool instance_;
    public static CarPool Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<CarPool>();
            return instance_;
        }
    }

    Stack<GameObject> pool = new Stack<GameObject>();
    public List<GameObject> prefabs;
    public float coolDown;
    float timer;
    public float topPosition;
    public float botPosition;
    Transform cars;
    void Start () {
        timer = coolDown;
        cars = GameObject.Find("Cars").transform;
	}

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0f)
        {
            timer = coolDown;
            GameObject go = GetObject();
            go.transform.parent = cars;
            int r = Random.Range(0, 2);
            if (r == 0)
            {
                DimetricTransform dT = go.GetComponent<DimetricTransform>();
                dT.position.z = topPosition;
                dT.position.x = 20;
                dT.position.y = 0;
                go.GetComponent<CarScript>().carSpeed = -10f;
            }
            else
            {
                DimetricTransform dT = go.GetComponent<DimetricTransform>();
                dT.position.z = botPosition;
                dT.position.x = 20;
                dT.position.y = 0;
                go.GetComponent<CarScript>().carSpeed = -15f;
            }
        }
    }

    public GameObject GetObject()
    {
        if (pool.Count > 0)
        {
            GameObject go = pool.Pop();
            go.SetActive(true);
            return go;
        }
        return Instantiate(prefabs[Random.Range(0,prefabs.Count)]) as GameObject;
    }

    public void PutObject(GameObject obj)
    {
        obj.SetActive(false);
        pool.Push(obj);
    }
}

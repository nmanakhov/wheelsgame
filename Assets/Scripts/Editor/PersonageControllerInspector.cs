﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor(typeof(PersonageController))]
public class PersonageControllerInspector : Editor {
    Vector3[] coordinates = new Vector3[5];
    void OnSceneGUI()
    {
        PersonageController personageController = (target as PersonageController);
        coordinates[0] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, personageController.topBorder));
        coordinates[1] = DimetricTransform.DimetricToWorldPosition(new Vector3(150, 0f, personageController.topBorder));
        coordinates[2] = DimetricTransform.DimetricToWorldPosition(new Vector3(150, 0f, personageController.botBorder));
        coordinates[3] = DimetricTransform.DimetricToWorldPosition(new Vector3(-150, 0f, personageController.botBorder));
        Handles.color = Color.red;
        Handles.DrawLine(coordinates[0], coordinates[1]);
        Handles.DrawLine(coordinates[2], coordinates[3]);
    }
}

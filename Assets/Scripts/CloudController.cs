﻿using UnityEngine;
using System.Collections;

public class CloudController : MonoBehaviour {
    public float borderRight;
    public float borderLeft;
    public float speed;
    void Start () {
	    
	}
	
	void Update () {
        this.transform.position += Vector3.right * speed * Time.deltaTime;
        if (this.transform.position.x > borderRight)
            transform.position = new Vector3(borderLeft,transform.position.y, transform.position.z);
	}
}

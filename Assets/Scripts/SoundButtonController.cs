﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundButtonController : MonoBehaviour {
    Toggle toggle;
    int isOn;
    void Start () {
        toggle = this.GetComponent<Toggle>();
        isOn = PlayerPrefs.GetInt("Music");
            toggle.isOn = (isOn == 0);
        SoundController.Instance.SetVolume(isOn);
    }

    public void TurnMusic()
    {
        if (toggle.isOn)
            isOn = 0;
        else isOn = 1; 
        PlayerPrefs.SetInt("Music",isOn);
        toggle.isOn = (isOn == 0);
        PlayerPrefs.Save();
        SoundController.Instance.SetVolume(isOn);
    }
   
}

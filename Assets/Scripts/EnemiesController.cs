﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemiesController : MonoBehaviour {
    public int maxCount = 3;
    public float topBorder;
    public float botBorder;
    public float rightBorder;
    List<GameObject> enemies = new List<GameObject>();
    public GameObject[] prefabs;
    public float speed;
    float timer=4f;
    private static EnemiesController instance_;
    public static EnemiesController Instance {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<EnemiesController>();
            return instance_;
        }
    }
    void Start () {
        Add();
	}
	
	void Update () {
        timer -= Time.deltaTime;
        if (timer < 0f && this.transform.childCount<maxCount)
        {
            timer = 4f;
            Add();
        }
	}

    public void Add()
    { 
        for (int i=0; i<maxCount - enemies.Count; i++){
            GameObject temp = Instantiate(prefabs[Random.Range(0,prefabs.Length)]);
            DimetricTransform dimetricTransform = temp.GetComponent<DimetricTransform>();
            dimetricTransform.marginLeft = true;
            dimetricTransform.position.x = Random.Range(0f, rightBorder);
            dimetricTransform.position.z = Random.Range(GetBotBorder(0), GetTopBorder(0));

            temp.transform.SetParent(this.transform);
            Enemy enemy = temp.GetComponent<Enemy>();
            enemy.speed = Random.Range(speed / 2, speed);
            enemy.i = 0;
            enemies.Add(temp);
        }

        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].GetComponent<Enemy>().i = i;
        }
    }

    public void Remove(GameObject obj)
    {
        enemies.Remove(obj);
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].GetComponent<Enemy>().i = i;
        }
    }

    public float GetBotBorder(int i)
    {
        if (enemies.Count == 0)
            return botBorder;
        return i*(topBorder-botBorder)/enemies.Count + botBorder;
    }

    public float GetTopBorder(int i)
    {
        if (enemies.Count == 0)
            return topBorder;
        return (i+1)* (topBorder - botBorder) / enemies.Count + botBorder;
    }

    public void GameOver()
    {
        for (int i = 0; i < enemies.Count; i++)
        {
            enemies[i].GetComponent<Enemy>().GameOver();
        }
        this.enabled = false;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class PersonagePicker : MonoBehaviour, IDragHandler, IPointerUpHandler, IPointerDownHandler {
    Transform[] childs;
    Vector3[] positions;
    public String[] triggers;
    public int[] prices;
    public int picked = 1;
    public int pickedPersonage = 1;
    public float speed = 10f;
    int movingWay=0; //-1:left; 0:stay; 1:right;
    int currentSprite =1;
    void Start () {
        PlayerPrefs.SetInt("boy", 1);
        childs = new Transform[3];
        positions = new Vector3[3];
        for (int i = 0; i < 3; i++)
        {
            childs[i] = transform.GetChild(i);
            positions[i] = childs[i].localPosition;
            
        }
        ResetSprites();
    }

	void Update () {
        switch (movingWay)
        {
            case 0:
                break;
            case -1:
                childs[(picked + 1) % 3].localPosition = Vector2.MoveTowards(childs[(picked + 1) % 3].localPosition, positions[1], speed*Time.deltaTime);
                childs[(picked) % 3].localPosition = Vector2.MoveTowards(childs[(picked) % 3].localPosition, positions[0], speed * Time.deltaTime);
                childs[(picked + 2) % 3].localPosition = positions[2];
                if (childs[(picked) % 3].localPosition == positions[0])
                { picked++; movingWay = 0; currentSprite++;
                    pickedPersonage++;
                    if (pickedPersonage >= triggers.Length)
                        pickedPersonage = 0;
                    CharacterSpawner.personNum = pickedPersonage;
                    ResetSprites();
                }
            break;
            case 1:
                childs[(picked + 2) % 3].localPosition = Vector2.MoveTowards(childs[(picked + 2) % 3].localPosition, positions[1], speed * Time.deltaTime);
                childs[(picked) % 3].localPosition = Vector2.MoveTowards(childs[(picked) % 3].localPosition, positions[2], speed * Time.deltaTime);
                childs[(picked + 1) % 3].localPosition = positions[0];
                if (childs[(picked) % 3].localPosition == positions[2])
                { picked--; movingWay = 0; currentSprite--;
                    pickedPersonage--;
                    if (pickedPersonage < 0)
                        pickedPersonage = triggers.Length - 1;
                    CharacterSpawner.personNum = pickedPersonage;
                    if (picked < 0)
                        picked = 2;
                    ResetSprites();
                }
            break;
        }
	}

    public void ResetSprites()
    {
        if (currentSprite -1< 0)
            currentSprite += triggers.Length;
        childs[(picked) % 3].GetComponent<Animator>().SetTrigger(triggers[currentSprite% triggers.Length]);
        childs[(picked+2) % 3].GetComponent<Animator>().SetTrigger(triggers[(currentSprite-1) % triggers.Length]);
        childs[(picked+1) % 3].GetComponent<Animator>().SetTrigger(triggers[(currentSprite + 1) % triggers.Length]);

        childs[(picked) % 3].GetChild(0).GetComponent<UnlockPersonageButton>().id = currentSprite % triggers.Length;
        childs[(picked + 2) % 3].GetChild(0).GetComponent<UnlockPersonageButton>().id = (currentSprite - 1) % triggers.Length;
        childs[(picked + 1) % 3].GetChild(0).GetComponent<UnlockPersonageButton>().id = (currentSprite + 1) % triggers.Length;

        childs[(picked) % 3].GetChild(0).GetComponent<Image>().enabled = (PlayerPrefs.GetInt(triggers[currentSprite % triggers.Length]) == 0);
        childs[(picked + 2) % 3].GetChild(0).GetComponent<Image>().enabled = (PlayerPrefs.GetInt(triggers[(currentSprite - 1) % triggers.Length]) == 0);
        childs[(picked + 1) % 3].GetChild(0).GetComponent<Image>().enabled = (PlayerPrefs.GetInt(triggers[(currentSprite + 1) % triggers.Length]) == 0);

        childs[(picked) % 3].GetChild(1).GetComponent<Text>().text = prices[currentSprite % triggers.Length].ToString();
        childs[(picked + 2) % 3].GetChild(1).GetComponent<Text>().text = prices[(currentSprite - 1) % triggers.Length].ToString();
        childs[(picked + 1) % 3].GetChild(1).GetComponent<Text>().text = prices[(currentSprite + 1) % triggers.Length].ToString();

    }
    public void MoveLeft() {
        if (movingWay==0)
        movingWay = -1;
    }

    public void MovingRight(){
        if (movingWay == 0)
            movingWay = 1;
    }
    float drag=0;
    public void OnDrag(PointerEventData eventData)
    {
        drag += eventData.delta.x;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (drag > 0)
            MovingRight();
        if (drag < 0)
            MoveLeft();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        drag = 0;
    }
}

﻿using UnityEngine;
using System.Collections;

public class CharacterSpawner : MonoBehaviour {
    public static int personNum =1;
    public string[] personsPaths;
    public GameObject gamePanel;
    public GameObject gameOverPanel;
	void Start () {
        Instantiate(Resources.Load(personsPaths[personNum]));    
	}
	
}

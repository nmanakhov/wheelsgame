﻿using UnityEngine;
using System.Collections;

public class ShakeAnimation : MonoBehaviour {
    public float maxAngle;
    public float speed = 1f;
	void Update () {
        transform.rotation = Quaternion.Euler(new Vector3(0f,0f,maxAngle*Mathf.Sin(speed*Time.time)));
	}
}

﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(DimetricTransform))]
[RequireComponent(typeof(Barrier))]
public class Barrier : MonoBehaviour {
    DimetricTransform dTransfrom;
    public float speed=2f;
    int id;
	void Start () {
        dTransfrom = this.GetComponent<DimetricTransform>();
	}
	
	
	void Update () {
        dTransfrom.Move(-Time.deltaTime* BarriersSpawner.Instance.speed, 0f, 0f);
        if (transform.position.x < -BarriersSpawner.Instance.spawningBorder)
            BarriersSpawner.Instance.PutToPool(this.gameObject);
	}

    public int Id
    {
        get {
            return id;
        }
        set {
            id = value;
        }
    }
}

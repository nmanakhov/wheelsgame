﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(DimetricTransform))]
public class Enemy : MonoBehaviour {
    EnemiesController enemiesController;
    Vector3 target;
    public float speed;
    public int i;
    DimetricTransform dimetricTransform;
    enum States { 
        moveTo, changeTarget, moveForward, fall, gameOver
    }
    States currentState = States.changeTarget;
	void Start () {
        enemiesController = EnemiesController.Instance;
        dimetricTransform = this.GetComponent<DimetricTransform>();
	}
    float timer;

	void Update () {
        if (timer>=0)
        timer -= Time.deltaTime;
	    switch(currentState)
        {
            case States.moveTo:
                dimetricTransform.Position = Vector3.MoveTowards(dimetricTransform.Position, target, speed*Time.deltaTime);
                if (Vector3.Magnitude(dimetricTransform.Position - target) < 0.01f)
                {
                    currentState = States.moveForward;
                    timer = Random.Range(0f,2f);
                }
            break;
            case States.moveForward:
            if (timer < 0f)
                currentState = States.changeTarget;
            break;
            case States.changeTarget:
                Vector3 temp = dimetricTransform.Position;
                temp.z = Random.Range(enemiesController.GetBotBorder(i),enemiesController.GetTopBorder(i));
                target = temp;
                currentState = States.moveTo;
            break;
            case States.fall:
                temp = dimetricTransform.Position;
                temp += Vector3.left*10f*Time.deltaTime;
                dimetricTransform.Position = temp;
            break;
            case States.gameOver:
                temp = dimetricTransform.Position;
                if (temp.x<100f) temp.x += speed* Time.deltaTime ;
                dimetricTransform.Position = temp;
            break;
        }
	}

    public void OnTriggerEnter(Collider coll)
    {
        switch (coll.tag)
        {
            case "Barrier":
                if (currentState == States.gameOver)
                    break;
                enemiesController.Remove(this.gameObject);
                GetComponent<Animator>().SetTrigger("Fall");
                currentState = States.fall;
                break;
            case "Bonus":
                GetComponent<Animator>().SetTrigger("MoveFast");
                BarriersSpawner.Instance.PutToPool(coll.gameObject);
                break;
        }
    }

    public void SelfDestroy()
    {
        Destroy(this.gameObject);
    }

    public void GameOver()
    {
        this.currentState = States.gameOver;
    }
}

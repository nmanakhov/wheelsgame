﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Charge : MonoBehaviour {
    private static Charge instance_;
    public static Charge Instance
    {
        get
        {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<Charge>();
            return instance_;
        }
    }

    public Image img;
    public float value = 100f;
    public float decreaseSpeed = 0.1f;
    PersonageController personageController;
    void Start()
    {
        img = GameObject.Find("Detector").GetComponent<Image>();
        personageController = this.GetComponent<PersonageController>();
        IncreaseCharge(99);
    }

    void Update()
    {
        if (value > 0)
            IncreaseCharge(-decreaseSpeed * Time.deltaTime);
        else
            personageController.Fall();
    }

    public void IncreaseCharge(float size)
    {
        value += size;
        img.fillAmount = value/100f;
        value = Mathf.Clamp(value, 0, 100f);
        if (value >= 100)
        {
            personageController.Boost();
            //Reset();
        } 
   }
    public void Reset()
    {
        value = 0;
        img.fillAmount = value / 100f;
    }
}

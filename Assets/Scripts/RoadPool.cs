﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RoadPool : MonoBehaviour {
    private static RoadPool instance_;
    public GameObject prefab;
    Transform roads;
    public static RoadPool Instance
    {
        get {
            if (instance_ == null)
                instance_ = GameObject.FindObjectOfType<RoadPool>();
            return instance_;
        }
    }

    Stack<GameObject> pool = new Stack<GameObject>();

    public void Start()
    {
        roads = GameObject.Find("Roads").transform;
        Debug.Log(roads==null);
    }

    public GameObject GetObject()
    {
        GameObject go = null;
        if (pool.Count > 0)
            go = pool.Pop();
        else go=Instantiate(prefab) as GameObject;
        go.transform.parent = roads;
        go.SetActive(true);
        return go;
    }

    public void PutObject(GameObject obj)
    {
        obj.SetActive(false);
        pool.Push(obj);
        obj.GetComponent<RoadScroller>().spawned = false;
    }
}

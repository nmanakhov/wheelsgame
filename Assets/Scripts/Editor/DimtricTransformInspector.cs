﻿using UnityEngine;
using System.Collections;
using UnityEditor;
[CustomEditor(typeof(DimetricTransform))]
public class DimtricTransformInspector : Editor {

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        DimetricTransform dt = (DimetricTransform)target;
        dt.UpdatePosition();
    }
}

﻿using UnityEngine;
using System.Collections;

public class RoadScroller : MonoBehaviour {
    public float rightBorder = 20f;
    public float leftBorder = -20f;
    public Vector3 distance = new Vector3(9.9f,0f,0f);
    BarriersSpawner barrierSpawner;
    public bool spawned = false;
    public void Start()
    {
        barrierSpawner = BarriersSpawner.Instance;
    }
    void Update () {
        Vector3 pos = this.transform.position;
        pos.x -= barrierSpawner.speed * Time.deltaTime;
        this.transform.position = pos;
        if (!spawned && this.transform.position.x < this.rightBorder)
        {
            GameObject temp = RoadPool.Instance.GetObject();
            temp.transform.position = this.transform.position + distance;
            spawned = true;
        }
        if (this.transform.position.x < this.leftBorder)
        {
            RoadPool.Instance.PutObject(this.gameObject);
        }
	}
}
